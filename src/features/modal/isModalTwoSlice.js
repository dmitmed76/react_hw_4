import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	isModalTwo: false,
}

export const isModalTwoSlice = createSlice({
	name: 'isModalTwo',
	initialState,
	reducers: {
		actionIsModalTwo: (state, action) => {
			state.isModalTwo = action.payload
		},
	}

})

export const { actionIsModalTwo } = isModalTwoSlice.actions
export default isModalTwoSlice.reducer
