import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	isModal: false,

}

export const isModalSlice = createSlice({
	name: 'isModal',
	initialState,
	reducers: {
		actionIsModal: (state, action) => {
			state.isModal = action.payload
		},
}
	
})

export const { actionIsModal } = isModalSlice.actions
export default isModalSlice.reducer
