import { configureStore } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import logger from "redux-logger";

import goodsSlice from '../features/goods/goodsSlice';
import isModalSlice from '../features/modal/isModalSlice';
import isModalTwoSlice from '../features/modal/isModalTwoSlice';
import isFavoriteSlice from '../features/favorites/favoritesSlice';


export const store = configureStore({
	reducer: {
		goods: goodsSlice,
		isModal: isModalSlice,
		isModalTwo: isModalTwoSlice,
		isFavorite: isFavoriteSlice
	},
	middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger, thunk)
})