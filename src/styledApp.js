import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import { ButtonStyle } from './components/GoodsCards/components/Card';

const AppWrapper = styled.div``

const MenuWrapper = styled.div`
display: flex;
align-items: center;
justify-content: center;
gap: 60px;
margin: 10px;
`

const LinkCustom = styled(NavLink)`
display: flex;
align-items: center;
justify-content: center;
text-decoration: none;
text-transform: uppercase;
color: #ffffff;
background-color: ${props => props.backgroundColor || 'darkblue'};
width: 140px;
height: 40px;
border-radius: 5px;
&.active {
background-color: grey;
  }
`

const ButtonModalWrapper = styled.div`
display: flex;
justify-content: space-around;
`

const ButtonModal = styled(ButtonStyle)`
background-color: ${props => props.backgroundColor || 'darkGreen'};
width: 150px;

`
export { AppWrapper, ButtonModalWrapper, ButtonModal, MenuWrapper, LinkCustom };