import styled from 'styled-components';

const HeaderStyle = styled.div`
display: grid;
grid-template-columns: 1fr 30px 30px 30px  30px 20px;
align-items: center;
justify-items: center;
background-color: darkBlue;
`

const HeaderTitle = styled.h2`
color: white;
text-align: center;
padding: 20px 0;
`
const SpanIcons = styled.span` 
color: white;
text-align: center;
`
export { HeaderStyle, HeaderTitle, SpanIcons };